<?php

	/*
		This is demonstration of code writing.
		This is simple-toy that should show colored and bordered objects on the display.
	*/

	require_once ( "core" . DIRECTORY_SEPARATOR . "Color.php" );
	require_once ( "core" . DIRECTORY_SEPARATOR . "IComponent.php" );
	require_once ( "core" . DIRECTORY_SEPARATOR . "RectangleBase.php" );
	require_once ( "core" . DIRECTORY_SEPARATOR . "Rectangles.php" );
	require_once ( "core" . DIRECTORY_SEPARATOR . "Renderer.php" );

	// 1) ...
	// Make & show a base obj

	// Make color
	$bg_color = new Color( 70, 255 );

	// Specify size
	$size_x = 100;
	$size_y = 30;

	// Create
	$rec = new RectangleBase( $bg_color, $size_x, $size_y );
	// Show
	$rec->render();


	// 2) ...
	// Make & show bordered obj

	// Redefine $bg_color
	$bg_color = new Color( 123, 124, 125 );

	// Create border color
	$br_color = new Color();

	// The same size
	$size_x = 100;
	$size_y = 30;

	// Show bordered obj
	$recb = new BorderedRectangle( $bg_color, $size_x, $size_y, $br_color );
	$recb->render();


	// 3) ...
	// Make & show positional obj

	// Specify position
	$pos_x = 2;
	$pos_y = 100;

	// Create
    $recp = new PositionedRectangle( $bg_color, $size_x, $size_y, $pos_x, $pos_y );
	// Show
	$recp->render();

	// 4) ...
	// Jast show all objs
	echo "<br/> Now show they all:";

	$obj_list = array ( $rec, $recb, $recp );

	$rend = new Renderer( $obj_list );
	$rend->print_all();

?>
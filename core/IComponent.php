<?php

	abstract class IComponent {

		protected $_color;
		protected $_width;
		protected $_height;

		abstract public function render();

		public function __construct( Color $color, $width = 10, $height = 10 ) {
			$this->_set_color( $color );
			$this->_set_width( $width );
			$this->_set_height( $height );
		}

		/* ********************************************************************
			Protected methods
		*/

		protected function _is_in_display_size( $size ) {
			if ( is_int( $size ) and $size >= 0 and $size < 1000 ) {
				return True;
			}
			
			return False;
		}

		/* ********************************************************************
			Private accessors
		*/

		private function _set_color( $color ) {
			$this->_color = $color;
		}

		private function _set_width( $width ) {
			if ( $this->_is_in_display_size( $width ) ) {
				$this->_width = $width;
			}
			else {
				$this->_bad_component_size( "WIDTH" );
			}
		}

		private function _set_height( $height ) {
			if ( $this->_is_in_display_size( $height ) ) {
				$this->_height = $height;
			}
			else {
				$this->_bad_component_size( "HEIGTH" );
			}
		}

		/* ********************************************************************
			Private methods
		*/

		private function _bad_component_size( $msg ) {
			throw new InvalidArgumentException( "$msg should be in range 0 .. 1000" );
		} 

	}

?>
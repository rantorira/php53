<?php

	class RectangleBase extends IComponent {
	
		private $_prototype;
		private $_native_style = 'font-size: 20px;';

		public function render() {
			$this->_make_prototype();
			echo $this->_prototype;
		}

		/* ********************************************************************
			Protected methods
		*/

		protected function _get_style() {
			return $this->_get_background_color() . ' ' . $this->_get_size();
		}

		/* ********************************************************************
			Private methods
		*/

		private function _make_prototype() {
			$this->_prototype = '<div ' . $this->_make_style() . '"> div </div>';
		}

		private function _make_style() {
			return 'style="' . $this->_native_style . ' ' . $this->_get_style() . '"';
		}

		private function _get_background_color() {
			return 'background-color:' . $this->_color->render() . ';';
		}
		
		private function _get_size() {
			return 'width:' . $this->_width . 'px; height:' . $this->_height . 'px;';
		}

	}

?>
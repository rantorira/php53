<?php

	class Color {
	
		private $_red;
		private $_green;
		private $_blue;

		public function __construct( $red = 0, $green = 0, $blue = 0 ) {
			$this->_set_red( $red );
			$this->_set_green( $green );
			$this->_set_blue( $blue );
		}

		public function render() {
			return "RGB(" . $this->_red . "," . $this->_green . "," . $this->_blue . ")";
		}
		
		/* ********************************************************************
			Private accessors
		*/

		private function _set_red ( $red ) {
			if ( $this->_is_looks_like_color( $red ) ) {
				$this->_red = $red;
			}
			else {
				$this->_bad_color( "RED" );
			}
		}

		private function _set_green( $green ) {
			if ( $this->_is_looks_like_color( $green ) ) {
				$this->_green = $green;
			}
			else {
				$this->_bad_color( "GREEN" );
			}
		}

		private function _set_blue( $blue ) {
			if ( $this->_is_looks_like_color( $blue ) ) {
				$this->_blue = $blue;
			}
			else {
				$this->_bad_color( "BLUE" );
			}
		}

		/* ********************************************************************
			Private methods
		*/

		private function _is_looks_like_color( $color ) {
			if ( is_int( $color ) and $color >= 0 and $color < 256 ) {
				return True;
			}
			
			return False;
		}
		
		private function _bad_color( $msg ) {
			throw new InvalidArgumentException( "$msg should be in range 0 .. 255 ");
		}

	}

?>
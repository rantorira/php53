<?php

	/*
		BorderedRectangle - obj that have colored solid border
	*/
	class BorderedRectangle extends RectangleBase {
		
		private $br_color;
		
		public function __construct( $color, $width, $height, Color $br_color ) {
			parent::__construct( $color, $width, $height );
			$this->_set_br_color( $br_color );
			
		}

		/* ********************************************************************
			Protected methods
		*/

		protected function _get_style() {
			$style = parent::_get_style();
			$style .= "border: solid; border-width: 1; border-color: " . $this->br_color->render() . ';';
			return $style;
		}

		/* ********************************************************************
			Private methods
		*/

		private function _set_br_color( $br_color ) {
			$this->br_color = $br_color;
		}

	} // BorderedRectangle


	/*
		PositionedRectangle - obj that have fixed position on the display
	*/
	class PositionedRectangle extends RectangleBase {

		private $_latitude;
		private $_longitude;

		public function __construct( $color, $width, $height, $latitude = 0, $longitude = 0 ) {
			parent::__construct( $color, $width, $height );
			$this->_set_latitude( $latitude );
			$this->_set_longitude( $longitude );
		}

		/* ********************************************************************
			Protected methods
		*/

		protected function _get_style() {
			$style = parent::_get_style();
			return $style . 'position: absolute; top: ' . $this->_latitude .'px; left: ' . $this->_longitude . 'px;';
		}

		/* ********************************************************************
			Private accessors
		*/

		private function _set_latitude( $latitude ) {
			if ( $this->_is_in_display_size( $latitude ) ) {
				$this->_latitude = $latitude;
			}
			else {
				$this->_bad_coordinates( "LATITUDE" );
			}
		}

		private function _set_longitude( $longitude ) {
			if ( $this->_is_in_display_size( $longitude ) ) {
				$this->_longitude = $longitude;
			}
			else {
				$this->_bad_coordinates( "LONGITUDE" );
			}		
		}

		/* ********************************************************************
			Private methods
		*/

		private function _bad_coordinates( $msg ) {
			throw new InvalidArgumentException( "$msg should be in range 0 .. 1000" );
		} 

	} // PositionedRectangle

?>
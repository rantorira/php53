<?php

	class Renderer {
		
		private $_objects_list;

		public function __construct( array $components ) {
			$this->_set_objects_list( $components );
		}

		public function print_all() {
			foreach ( $this->_objects_list as $object ) {
				$object->render();
			}
		}

		/* ********************************************************************
			Private accessors
		*/

		private function _set_objects_list( $components ) {
			$this->_is_objects_list( $components );
			$this->_objects_list = $components;
		}

		/* ********************************************************************
			Private methods
		*/

		private function _is_objects_list( $components ) {

			$index = 0;
			foreach ( $components as $component ) {

				$index++;

				if ( !( $component instanceof IComponent ) ) {
					$this->_bad_components( $index );
				}
			}
		}

		private function _bad_components( $msg ) {
			throw new InvalidArgumentException( "Incorrect object with index: $msg" );
		}

	}

?>